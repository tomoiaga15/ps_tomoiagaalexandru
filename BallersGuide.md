﻿# BallersGuide

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)


### Descriere

- Aplicatia ofera posibilitate utilizatorilor de a se inregistra, loga si accesa date din aceasta
- Userul are posibilitatea de a crea, sterge, vizualiza si actualiza anumite date din baza de date in functie de rolul si drepturile pe care le are user-ul respectiv


### Studiul proiectului

- Proiectul se bazeaza pe baschet, un sport de echipa cu diferite constrangeri, in acest caz, una dintre acestea este ca exista 5 pozitii de joc, acestea sunt : Center, Power Forward, Small Forward, Pointing Guard, Shooting Guard.
- O alta constrangere este faptul ca intr-o echipa nu pot exista mai mult de 3 jucatori pe aceeasi pozitie insa nu impune sa fie minim un jucator pe fiecare pozitie, acestea putand fii schimbate.
- in momentul crearii unei echipe, aceasta nu are jucatori, pe parcurs, aceasta este populata, conditia ca un jucator sa fie eliminat din echipa este ca aceasta sa ramana cu minim 5 jucatori.
- in momentul selectarii unei pozitii, unui jucator i se poate atribui o pozitie doar daca inaltimea lui se afla in intervalele necesare pozitiei


### Structura

- proiectul este structurat in pachetul de model, unde se afla toate clasele reprezentative pentru model, majoritatea dintre acestea aflandu-se si in baza de date ca tabele
- pachetul de controler unde se afla logica aplicatie
- DAO-urile pentru fiecare clasa care are legatura cu baza de date
- legatura in baza de date se face intre tabele in majoritatea cazurilor ca fiind 1-on-1
- cele 3 tipuri de useri utilizati au atribuite cate un rol fiecare, fiecare rol avand anumite drepturi
- aceste roluri sunt atribuite initial ca find user normal, putand fii modificare doar de admin
- adminul este hard-coded initial, pentru a avea posibilitatea de a alege cele mai potrivite drepturi
- userul player este creat in momentul in care se adauga un player nou, avand o parola default si numele fiind cel personal
- cand un user este setat din player in alt rol, acesta este sters din lista de playeri
- cand un user este setat ca player, acesta este adaugat in lista de playeri



### Use case
- Exista 3 tipuri de useri
--Admin : 
~ poate face CRUD pe toate datele legate de jucatori
~ poate face CRUD pe toate datele legate de user
--User : 
~poate face CRUD pe toate datele legate de jucatori
~poate face CRUD pe toate datele personale
--Player :
~poate face CRUD pe toate datele personale

### Patternuri folosite

- Conexiunea la baza de date s-a facut prin SpringBoot, aceasta fiind o conexiune singleton.
- Am ales sa foloses Factory design pattern pentru pozitii deoarece acestea sunt mai multe la numar si au o diferenta atat la descriere cat si la constrangerile de creere a unui jucator dintr-un anumit tip.
- Am folosit observer pentru a notifica jucatorii ca li s-a adaugat un coechipier nou in echipa, fiecare din apartinatorii echipei afiseaza un mesaj cum ca un nou player a fost adaugat in echipa de care apartin.

### BackEnd

- In backend exista toate modelele folosite, controlerele pentru fiecare model si api pentru fiecare comanda. 
- In aplicatie se pot face requesturi de tipul GET, POST, PUT, DELETE, acestea sunt tratate pentru fiecare model in partea.
- Toate aceste api sunt folosite in frontend pentru a putea trimite requesturile dorite de utilizator

### FrontEnd
- Pentru frontend am ales sa am 6 pagini, una principala de index unde se deschide initial proiectul iar celelalte 5 pentru interactiunea cu paginile.
- In prima pagina este afisata o mica istorie a baschetului, in partea de sus a paginii, atat in acest caz cat si in celelate, exista butoanele de navigatie intre pagini.
- O alta pagina cuprinde datele legate de jucatori. In aceasta pagina pot avea loc atat adaugari cat si stergeri, sau actualizari la jucatori.
- O a treia pagina este pagina de pozitii in care sunt prezente datele despre pozitiile posibile in jocul de baschet, acestea se pot altera dupa dorinta utilizatorului.
- O alta pagina este pagina de useri in care se poate adauga un cont nou al unui user si se pot face modificari pe acestea.
- A cincea pagina este pagina de roluri, fiecarui user ii este atribui un rol in cadrul aplicatiei si printr rolul respectiv ii este descris ce poate face userul si ce nu poate.
- Ultima pagina este pagina de drepturi, acestea sunt specifice fiecarui rol, un anumit rol are anumite proprietati in timp ce altul nu are.

### Diagrams

  - Use case
![N|Solid](https://i.ibb.co/4N7RMSS/2019-03-22-1.png)
  - Class Diagram
![N|Solid](https://i.ibb.co/TmLpWdT/2019-03-22-2.png)
  - Package Diagram
![N|Solid](https://i.ibb.co/ZHHcDK1/2019-03-22-3.png)
  - DB Diagram
![N|Solid](https://i.ibb.co/80X4Vjd/2019-03-22.png)


### Utils

- IDE folosit este IntelliJ
- pentru baza de date am folosit MySQL Workbench
- xampp a fost folosit pentru conexiune
- pentru frontend am folosit jquery
- pentru salvarea sursei si pentru urmarirea dezvolatii aplicatiei s-a folosit bitbucket



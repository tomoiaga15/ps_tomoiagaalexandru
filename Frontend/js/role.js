$(document).ready(function()    {
	
	var $name = $("#name");
	var $roles = $('#roles');
	var $data_id = $('#data_id');
	
        $.ajax({
            type: 'GET',
            url: 'http://localhost:8080/Role/display',
			dataType:"json",
            success: function(roles){
                $.each(roles, function(i, role){
					$roles.append('<li>'+
								'<p><strong> ID: </strong>' + role.id + '</p>' +
								'<p><strong> Nume: </strong>' + role.name + '</p>' +
								'<button id = "remove" data_id = ' + role.id + '>X</button>' +
								'<button id = "update" data_id = ' + role.id + '>Update</button></li>');
            });
        },
			error : function() {
				alert('error loading roles');
			}
    });

	$("#add").on('click', function()    {
		var role = {
			"name" : $name.val()
		}
        $.ajax({
            type: 'POST',
            url: 'http://localhost:8080/Role/add',
			data: JSON.stringify(role),
			dataType: 'json',
			headers: {
				'Content-Type': 'application/json',
				'Accept' : "*/*",
			},
            success: function(newRole){
					$roles.append('<li>'+
								'<p><strong> ID: </strong>' + newRole.id + '</p>' +
								'<p><strong> Nume: </strong>' + newRole.name + '</p>' +
								'<button id = "remove" data_id = ' + newRole.id + '>X</button>' +
								'<button id = "update" data_id = ' + newRole.id + '>Update</button></li>');
        },
			error : function() {
				alert('error adding role');
			}
		});
	});
	
	$roles.delegate('#remove','click', function()    {
		
		var $li = $(this).closest('li');
		
		$.ajax({
			type: 'DELETE',
			url : 'http://localhost:8080/Role/remove/'+$(this).attr('data_id'),
			success: function (){
				$li.fadeOut(500, function() {
					$(this).remove();
			})
			},
			error : function() {
				alert('error removing role');
			}
		});
	});
	
	$roles.delegate('#update','click', function()    {
		
		var $li = $(this).closest('li');
		var role = {
			"id" : $(this).attr('data_id'),
			"name" : $name.val()
		}
		
		$.ajax({
			type: 'PUT',
			url : 'http://localhost:8080/Role/update/',
			data: JSON.stringify(role),
			dataType: 'json',
			headers: {
				'Content-Type': 'application/json',
				'Accept' : "*/*",
			},
			success: function (newRole){
				$li.remove();
				$roles.append('<li>'+
								'<p><strong> ID: </strong>' + newRole.id + '</p>' +
								'<p><strong> Nume: </strong>' + newRole.name + '</p>' +
								'<button id = "remove" data_id = ' + newRole.id + '>X</button>' +
								'<button id = "update" data_id = ' + newRole.id + '>Update</button></li>');
			},
			error : function() {
				alert('error update role');
			}
		});
	});
});
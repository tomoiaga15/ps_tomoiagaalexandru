$(document).ready(function()    {
	
	var $name = $("#name");
	var $positions = $('#positions');
	var $data_id = $('#data_id');
	
        $.ajax({
            type: 'GET',
            url: 'http://localhost:8080/Position/display',
			dataType:"json",
            success: function(positions){
                $.each(positions, function(i, position){
					$positions.append('<li>'+
								'<p><strong> ID: </strong>' + position.id + '</p>' +
								'<p><strong> Nume: </strong>' + position.name + '</p>' +
								'<button id = "remove" data_id = ' + position.id + '>X</button>' +
								'<button id = "update" data_id = ' + position.id + '>Update</button></li>');
            });
        },
			error : function() {
				alert('error loading positions');
			}
    });

	$("#add").on('click', function()    {
		var position = {
			"name" : $name.val()
		}
        $.ajax({
            type: 'POST',
            url: 'http://localhost:8080/Position/add',
			data: JSON.stringify(position),
			dataType: 'json',
			headers: {
				'Content-Type': 'application/json',
				'Accept' : "*/*",
			},
            success: function(newPosition){
					$positions.append('<li>'+
								'<p><strong> ID: </strong>' + newPosition.id + '</p>' +
								'<p><strong> Nume: </strong>' + newPosition.name + '</p>' +
								'<button id = "remove" data_id = ' + newPosition.id + '>X</button>' +
								'<button id = "update" data_id = ' + newPosition.id + '>Update</button></li>');
        },
			error : function() {
				alert('error adding Position');
			}
		});
	});
	
	$positions.delegate('#remove','click', function()    {
		
		var $li = $(this).closest('li');
		
		$.ajax({
			type: 'DELETE',
			url : 'http://localhost:8080/Position/remove/'+$(this).attr('data_id'),
			success: function (){
				$li.fadeOut(500, function() {
					$(this).remove();
			})
			},
			error : function() {
				alert('error removing Position');
			}
		});
	});
	
	$positions.delegate('#update','click', function()    {
		
		var $li = $(this).closest('li');
		var position = {
			"id" : $(this).attr('data_id'),
			"name" : $name.val()
		}
		
		$.ajax({
			type: 'PUT',
			url : 'http://localhost:8080/Position/update/',
			data: JSON.stringify(position),
			dataType: 'json',
			headers: {
				'Content-Type': 'application/json',
				'Accept' : "*/*",
			},
			success: function (newPosition){
				$li.remove();
				$positions.append('<li>'+
								'<p><strong> ID: </strong>' + newPosition.id + '</p>' +
								'<p><strong> Nume: </strong>' + newPosition.name + '</p>' +
								'<button id = "remove" data_id = ' + newPosition.id + '>X</button>' +
								'<button id = "update" data_id = ' + newPosition.id + '>Update</button></li>');
			},
			error : function() {
				alert('error update Position');
			}
		});
	});
});
$(document).ready(function()    {
	
	var $name = $("#name");
	var $rights = $('#rights');
	var $data_id = $('#data_id');
	
        $.ajax({
            type: 'GET',
            url: 'http://localhost:8080/Right/display',
			dataType:"json",
            success: function(rights){
                $.each(rights, function(i, right){
					$rights.append('<li>'+
								'<p><strong> ID: </strong>' + right.id + '</p>' +
								'<p><strong> Nume: </strong>' + right.name + '</p>' +
								'<button id = "remove" data_id = ' + right.id + '>X</button>' +
								'<button id = "update" data_id = ' + right.id + '>Update</button></li>');
            });
        },
			error : function() {
				alert('error loading rights');
			}
    });

	$("#add").on('click', function()    {
		var right = {
			"roles": [],
			"name" : $name.val()
		}
        $.ajax({
            type: 'POST',
            url: 'http://localhost:8080/Right/add',
			data: JSON.stringify(right),
			dataType: 'json',
			headers: {
				'Content-Type': 'application/json',
				'Accept' : "*/*",
			},
            success: function(newRight){
					$rights.append('<li>'+
								'<p><strong> ID: </strong>' + newRight.id + '</p>' +
								'<p><strong> Nume: </strong>' + newRight.name + '</p>' +
								'<button id = "remove" data_id = ' + newRight.id + '>X</button>' +
								'<button id = "update" data_id = ' + newRight.id + '>Update</button></li>');
        },
			error : function() {
				alert('error adding Right');
			}
		});
	});
	
	$rights.delegate('#remove','click', function()    {
		
		var $li = $(this).closest('li');
		
		$.ajax({
			type: 'DELETE',
			url : 'http://localhost:8080/Right/remove/'+$(this).attr('data_id'),
			success: function (){
				$li.fadeOut(500, function() {
					$(this).remove();
			})
			},
			error : function() {
				alert('error removing Right');
			}
		});
	});
	
	$rights.delegate('#update','click', function()    {
		
		var $li = $(this).closest('li');
		var right = {
			"id" : $(this).attr('data_id'),
			"roles": [],
			"name" : $name.val()
		}
		
		$.ajax({
			type: 'PUT',
			url : 'http://localhost:8080/Right/update/',
			data: JSON.stringify(right),
			dataType: 'json',
			headers: {
				'Content-Type': 'application/json',
				'Accept' : "*/*",
			},
			success: function (newRight){
				$li.remove();
				$rights.append('<li>'+
								'<p><strong> ID: </strong>' + newRight.id + '</p>' +
								'<p><strong> Nume: </strong>' + newRight.name + '</p>' +
								'<button id = "remove" data_id = ' + newRight.id + '>X</button>' +
								'<button id = "update" data_id = ' + newRight.id + '>Update</button></li>');
			},
			error : function() {
				alert('error update Right');
			}
		});
	});
});
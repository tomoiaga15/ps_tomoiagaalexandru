$(document).ready(function()    {
	
	var $name = $("#name");
	var $age = $("#age");
	var $size = $("#size");
	var $positionId = $("#positionId");
	var $number = $("#number");
	var $players = $('#players');
	var $data_id = $('#data_id');
	
        $.ajax({
            type: 'GET',
            url: 'http://localhost:8080/player',
			dataType:"json",
            success: function(players){
                $.each(players, function(i, player){
					$players.append('<li>'+
								'<p><strong> ID: </strong>' + player.id +'</p>' +
								'<p><strong> Nume: </strong>' + player.name + '</p>' +
								'<p><strong> Age in league: </strong>' + player.ageInLeague + '</p>' +
								'<p><strong> Position: </strong>' + player.position.id + '</p>' +
								'<p><strong> Size: </strong>' + player.size + '</p>' +
								'<p><strong> Number: </strong>' + player.number + '</p>' +
								'<button id = "remove" data_id = ' + player.id + '>X</button>' +
								'<button id = "update" data_id = ' + player.id + '>Update</button></li>');
            });
        },
			error : function() {
				alert()
				alert('error loading players');
			}
    });

	$("#add").on('click', function()    {
		var player = {
			"name" : $name.val(),
			"ageInLeague": $age.val(),
			"size": $size.val(),
			"position": {
				"id": $positionId.val()
			},
			"number": $number.val()
		}
        $.ajax({
            type: 'POST',
            url: 'http://localhost:8080/player/add',
			data: JSON.stringify(player),
			dataType: 'json',
			headers: {
				'Content-Type': 'application/json',
				'Accept' : "*/*",
			},
            success: function(newPlayer){
					$players.append('<li>'+
								'<p><strong> ID: </strong>' + newPlayer.id +'</p>' +
								'<p><strong> Nume: </strong>' + newPlayer.name + '</p>' +
								'<p><strong> Age in league: </strong>' + newPlayer.ageInLeague + '</p>' +
								'<p><strong> Position: </strong>' + newPlayer.position.id + '</p>' +
								'<p><strong> Size: </strong>' + newPlayer.size + '</p>' +
								'<p><strong> Number: </strong>' + newPlayer.number + '</p>' +
								'<button id = "remove" data_id = ' + newPlayer.id + '>X</button>' +
								'<button id = "update" data_id = ' + newPlayer.id + '>Update</button></li>');
        },
			error : function() {
				alert('error adding Player');
			}
		});
	});
	
	$players.delegate('#remove','click', function()    {
		
		var $li = $(this).closest('li');
		
		$.ajax({
			type: 'DELETE',
			url : 'http://localhost:8080/player/remove/'+$(this).attr('data_id'),
			success: function (){
				$li.fadeOut(500, function() {
					$(this).remove();
			})
			},
			error : function() {
				alert('error removing Player');
			}
		});
	});
	
	$players.delegate('#update','click', function()    {
		
		var $li = $(this).closest('li');
		var player = {
			"id" : $(this).attr('data_id'),
			"name" : $name.val(),
			"ageInLeague": $age.val(),
			"size": $size.val(),
			"position": {
				"id": $positionId.val()
			},
			"number": $number.val()
		}
		
		$.ajax({
			type: 'PUT',
			url : 'http://localhost:8080/player/update/',
			data: JSON.stringify(player),
			dataType: 'json',
			headers: {
				'Content-Type': 'application/json',
				'Accept' : "*/*",
			},
			success: function (newPlayer){
				$li.remove();
				$players.append('<li>'+
								'<p><strong> ID: </strong>' + newPlayer.id +'</p>' +
								'<p><strong> Nume: </strong>' + newPlayer.name + '</p>' +
								'<p><strong> Age in league: </strong>' + newPlayer.ageInLeague + '</p>' +
								'<p><strong> Position: </strong>' + newPlayer.position.id + '</p>' +
								'<p><strong> Size: </strong>' + newPlayer.size + '</p>' +
								'<p><strong> Number: </strong>' + newPlayer.number + '</p>' +
								'<button id = "remove" data_id = ' + newPlayer.id + '>X</button>' +
								'<button id = "update" data_id = ' + newPlayer.id + '>Update</button></li>');
			},
			error : function() {
				alert('error update Player');
			}
		});
	});
});
$(document).ready(function()    {
	
	var $name = $("#name");
	var $lname = $("#lname");
	var $email = $("#email");
	var $pass = $("#pass");
	var $users = $("#users");
	var $data_id = $("#data_id");
	
        $.ajax({
            type: 'GET',
            url: 'http://localhost:8080/user/display',
			dataType:"json",
            success: function(users){
                $.each(users, function(i, user){
					$users.append('<li>'+
								'<p><strong> ID: </strong>' + user.id + '</p>' +
								'<p><strong> Nume: </strong>' + user.name + '</p>' +
								'<p><strong> Last Name: </strong>' + user.lastName + '</p>' +
								'<p><strong> Email: </strong>' + user.email + '</p>' +
								'<button id = "remove" data_id = ' + user.id + '>X</button>' +
								'<button id = "update" data_id = ' + user.id + '>Update</button></li>');
            });
        },
			error : function() {
				alert('error loading users');
			}
    });

	$("#add").on('click', function()    {
		var user = {
        "email": $email.val(),
        "password": $pass.val(),
        "name": $name.val(),
        "lastName": $lname.val(),
        "role": {
            "id": 10,
            "name": "User"
        }
        };
		
        $.ajax({
            type: 'POST',
            url: 'http://localhost:8080/user/add',
			data: JSON.stringify(user),
			dataType: 'json',
			headers: {
				'Content-Type': 'application/json',
				'Accept' : "*/*",
			},
            success: function(newUser){
					$users.append('<li>'+
								'<p><strong> ID: </strong>' + newUser.id + '</p>' +
								'<p><strong> Nume: </strong>' + newUser.name + '</p>' +
								'<p><strong> Last Name: </strong>' + newUser.lastName + '</p>' +
								'<p><strong> Email: </strong>' + newUser.email + '</p>' +
								'<button id = "remove" data_id = ' + newUser.id + '>X</button>' +
								'<button id = "update" data_id = ' + newUser.id + '>Update</button></li>');
        },
			error : function(newUser) {
				alert('error adding User');
			}
		});
	});
	
	$users.delegate('#remove','click', function()    {
		
		var $li = $(this).closest('li');
		
		$.ajax({
			type: 'DELETE',
			url : 'http://localhost:8080/user/remove/'+$(this).attr('data_id'),
			success: function (){
				$li.fadeOut(500, function() {
					$(this).remove();
			})
			},
			error : function() {
				alert('error removing User');
			}
		});
	});
	
	$users.delegate('#update','click', function()    {
		
		var $li = $(this).closest('li');
		var user = {
			"id" : $(this).attr('data_id'),
			"email": $email.val(),
        "password": $pass.val(),
        "name": $name.val(),
        "lastName": $lname.val(),
        "role": {
            "id": 10,
            "name": "User"
        }
		}
		
		$.ajax({
			type: 'PUT',
			url : 'http://localhost:8080/user/update/',
			data: JSON.stringify(user),
			dataType: 'json',
			headers: {
				'Content-Type': 'application/json',
				'Accept' : "*/*",
			},
			success: function (newUser){
				$li.remove();
				$users.append('<li>'+
								'<p><strong> ID: </strong>' + newUser.id + '</p>' +
								'<p><strong> Nume: </strong>' + newUser.name + '</p>' +
								'<p><strong> Last Name: </strong>' + newUser.lastName + '</p>' +
								'<p><strong> Email: </strong>' + newUser.email + '</p>' +
								'<button id = "remove" data_id = ' + newUser.id + '>X</button>' +
								'<button id = "update" data_id = ' + newUser.id + '>Update</button></li>');
			},
			error : function() {
				alert('error update User');
			}
		});
	});
});
package com.example.demo.controller;

import com.example.demo.models.Role;
import com.example.demo.models.data.RoleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("Role")
@EnableAutoConfiguration
public class RoleController {


    @Autowired
    private RoleDao roleDao;

    //Process for displaying list of roles

    @RequestMapping(value = "display", method = RequestMethod.GET)
    @ResponseBody
    public Iterable<Role> displayeRoleList() {
        return roleDao.findAll();

    }

    //Process for adding role to DB

    @RequestMapping(value = "add", method = RequestMethod.POST)
    @ResponseBody
    public Role processAddRole(@RequestBody(required = false) Role newRole) {
        roleDao.save(newRole);
        //positionService.addPosition(newPosition);
        return newRole;
    }

    //Process for removing role from DB

    @RequestMapping(value = "remove/{roleId}", method = RequestMethod.DELETE)
    @ResponseBody
    public Role processRemoveRole(@PathVariable("roleId") int roleId) {
        Role newRole = roleDao.findOne(roleId);
        roleDao.delete(roleId);
        //positionService.removePosition(positionId);
        return newRole;
    }

    //Process for updating existing role in DB
    @RequestMapping(value = "update", method = RequestMethod.PUT)
    @ResponseBody
    public Role processUpdateRole(@RequestBody(required = false) Role role) {

        Role newRole = roleDao.findOne(role.getId());
        newRole.setName(role.getName());
        roleDao.save(newRole);

        //positionService.updatePosition(position.getId(), position.getName());
        return role;
    }

}

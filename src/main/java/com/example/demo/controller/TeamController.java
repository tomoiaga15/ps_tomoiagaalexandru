package com.example.demo.controller;


import com.example.demo.models.Player;
import com.example.demo.models.Team;
import com.example.demo.models.data.PlayerDao;
import com.example.demo.models.data.TeamDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("Team")
@EnableAutoConfiguration
public class TeamController {

        @Autowired
        private TeamDao teamDao;

        @Autowired
        private PlayerDao playerDao;


        //Process for displaying list of teams

        @RequestMapping(value = "display", method = RequestMethod.GET)
        @ResponseBody
        public Iterable<Team> displayeTeamList() {
            return teamDao.findAll();
        }

        //Process for adding team to DB

        @RequestMapping(value = "add", method = RequestMethod.POST)
        @ResponseBody
        public Team processAddTeam(@RequestBody(required = false) Team newTeam) {
            teamDao.save(newTeam);
            //teamService.addTeam(newTeam);
            return newTeam;
        }

        //Process for removing team from DB

        @RequestMapping(value = "remove/{teamId}", method = RequestMethod.DELETE)
        @ResponseBody
        public Team processRemoveTeam(@PathVariable("teamId") int teamId) {

            Team team = teamDao.findOne(teamId);
            for(Player p : team.getPlayers()){
                team.removePlayer(p);
            }
            teamDao.delete(team);
            //teamService.removeTeam(TeamId);
            return team;
        }

        //Process for updating existing team in DB

    @RequestMapping(value = "update", method = RequestMethod.PUT)
        @ResponseBody
        public Team processUpdateTeam(@RequestBody(required = false) Team team) {

            Team newTeam = teamDao.findOne(team.getId());
            newTeam.setName(team.getName());
            teamDao.save(newTeam);

            //teamService.updateTeam(TeamId, name);

            return newTeam;
        }

        @RequestMapping(value = "addPlayer/{teamId}", method = RequestMethod.PUT)
        @ResponseBody
        public Team processAddPlayerToTeam(@RequestBody(required = false) int idPlayer, @PathVariable int teamId){
            Player player = playerDao.findOne(idPlayer);
            Team team = teamDao.findOne(teamId);
            team.addPlayer(player);
            teamDao.save(team);
            return team;
        }
}

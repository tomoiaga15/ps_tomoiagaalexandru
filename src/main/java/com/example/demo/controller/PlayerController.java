package com.example.demo.controller;

import com.example.demo.models.Player;
import com.example.demo.models.User;
import com.example.demo.models.data.PlayerDao;
import com.example.demo.models.data.PositionDao;
import com.example.demo.models.data.RoleDao;
import com.example.demo.models.data.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.MessageDigest;
import java.util.Random;

@Controller
@RequestMapping("player")
@EnableAutoConfiguration
public class PlayerController {

    @Autowired
    private PlayerDao playerDao;
    @Autowired
    private PositionDao positionDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;


    //Process for displaying list of players

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public Iterable<Player> displayePlayerList() {
        return playerDao.findAll();
    }

    //Process for adding player to DB

    @RequestMapping(value = "add", method = RequestMethod.POST)
    @ResponseBody
    public Player processAddPlayer(@RequestBody(required = false) Player newPlayer) {
        /*try{
        playerService.addPlayer(newPlayer);
        }
        catch(Exception e){
            System.out.println("Inaltime invalida");
        }*/

    User user = new User();
    user.setName(newPlayer.getName());
    user.setLastName(newPlayer.getName());
    user.setEmail("payer"+newPlayer.getNumber()+newPlayer.getAgeInLeague()+"@email.com");
    user.setRole(roleDao.findOne(10));
    Random random = new Random();
    user.setSalt(random.nextInt());
    user.setPassword(encodePassword("Parola1", user.getSalt()));
    if(!exist(user)) {
        userDao.save(user);
        playerDao.save(newPlayer);
    return newPlayer;}
    return null;
    }

    //Process for removing player from DB

    @RequestMapping(value = "remove/{playerId}", method = RequestMethod.DELETE)
    @ResponseBody
    public Player processRemovePlayer(@PathVariable("playerId") int playerId) {
    Player player = playerDao.findOne(playerId);
    playerDao.delete(playerId);
    //playerService.removePlayer(playerId);
    return player;
    }

    //Process for updating existing player in DB

    @RequestMapping(value = "update", method = RequestMethod.PUT)
    @ResponseBody
    public Player processUpdatePlayer(@RequestBody(required = false) Player newPlayer) {

        Player player = playerDao.findOne(newPlayer.getId());
        player.setAgeInLeague(newPlayer.getAgeInLeague());
        player.setSize(newPlayer.getSize());
        player.setName(newPlayer.getName());
        player.setNumber(newPlayer.getNumber());
        player.setPosition(positionDao.findOne(newPlayer.getPosition().getId()));
        playerDao.save(player);

        //playerService.updatePlayer(playerId, newPlayer);
        return player;
    }

    //Function for checking if other user with same email exists

    private Boolean exist(User newUser){
        Iterable<User> users = userDao.findAll();
        for(User user:users)
            if(user.getEmail().equals(newUser.getEmail())){
                return true;
            }
        return false;
    }

    //Function for encoding the password of user

    private String encodePassword(String password, int salt) {
        try {
            password+=salt;
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(password.getBytes("UTF-8"));
            StringBuilder hexString = new StringBuilder();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

}
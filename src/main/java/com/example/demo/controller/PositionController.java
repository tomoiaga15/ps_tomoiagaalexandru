package com.example.demo.controller;


import com.example.demo.models.Position;
import com.example.demo.models.data.PositionDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("Position")
@EnableAutoConfiguration
public class PositionController {

    @Autowired
    private PositionDao positionDao;

    //Process for displaying list of Positions

    @RequestMapping(value = "display", method = RequestMethod.GET)
    @ResponseBody
    public Iterable<Position> displayePositionList() {
        return positionDao.findAll();
    }

    //Process for adding position to DB

    @RequestMapping(value = "add", method = RequestMethod.POST)
    @ResponseBody
    public Position processAddPosition(@RequestBody(required = false) Position newPosition) {
        positionDao.save(newPosition);
        //positionService.addPosition(newPosition);
        return newPosition;
    }

    //Process for removing position from DB

    @RequestMapping(value = "remove/{positionId}", method = RequestMethod.DELETE)
    @ResponseBody
    public Position processRemovePosition(@PathVariable("positionId") int positionId) {
        Position newPosition = positionDao.findOne(positionId);
        positionDao.delete(positionId);
        //positionService.removePosition(positionId);
        return newPosition;
    }

    //Process for updating existing position in DB
    @RequestMapping(value = "update", method = RequestMethod.PUT)
    @ResponseBody
    public Position processUpdatePosition(@RequestBody(required = false) Position position) {

        Position newPosition = positionDao.findOne(position.getId());
        newPosition.setName(position.getName());
        positionDao.save(newPosition);

        //positionService.updatePosition(position.getId(), position.getName());
        return position;
    }

}

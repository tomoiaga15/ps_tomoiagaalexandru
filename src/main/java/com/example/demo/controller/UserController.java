package com.example.demo.controller;

import com.example.demo.models.Player;
import com.example.demo.models.User;
import com.example.demo.models.data.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.MessageDigest;
import java.util.Random;

@Controller
@RequestMapping("user")
@EnableAutoConfiguration
public class UserController {

    @Autowired
    private UserDao userDao;
    //Process for displaying list of users
    @RequestMapping(value = "display", method = RequestMethod.GET)
    @ResponseBody
    public Iterable<User> displayeUserList() {
        return userDao.findAll();
    }

    //Process for adding user to DB

    @RequestMapping(value = "add", method = RequestMethod.POST)
    @ResponseBody
    public User processAddUser(@RequestBody(required = false) User newUser) {
        /*try{
        playerService.addPlayer(newPlayer);
        }
        catch(Exception e){
            System.out.println("Inaltime invalida");
        }*/
        Random random = new Random();
        newUser.setSalt(random.nextInt());
        newUser.setPassword(encodePassword(newUser.getPassword(), newUser.getSalt()));
        if (!exist(newUser)) {
            userDao.save(newUser);
            return newUser;
        }
        return null;
    }

    //Process for removing user from DB

    @RequestMapping(value = "remove/{userId}", method = RequestMethod.DELETE)
    @ResponseBody
    public User processRemoveUser(@PathVariable("userId") int userId) {
        User user = userDao.findOne(userId);
        userDao.delete(userId);
        //playerService.removePlayer(playerId);
        return user;
    }

    //Process for updating existing user in DB

    @RequestMapping(value = "update", method = RequestMethod.PUT)
    @ResponseBody
    public User processUpdateUser(@RequestBody(required = false) User newUser) {

        User editedUser = userDao.findOne(newUser.getId());
        editedUser.setName(newUser.getName());
        editedUser.setLastName(newUser.getLastName());
        editedUser.setEmail(newUser.getEmail());
        userDao.save(editedUser);

        //playerService.updatePlayer(playerId, newPlayer);
        return editedUser;
    }

    //Function for checking if other user with same email exists

    private Boolean exist(User newUser){
        Iterable<User> users = userDao.findAll();
        for(User user:users)
            if(user.getEmail().equals(newUser.getEmail())){
                return true;
            }
        return false;
    }

    //Function for encoding the password of user

    private String encodePassword(String password, int salt) {
        try {
            password+=salt;
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(password.getBytes("UTF-8"));
            StringBuilder hexString = new StringBuilder();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

}

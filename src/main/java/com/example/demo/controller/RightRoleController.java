package com.example.demo.controller;

import com.example.demo.models.RightRole;
import com.example.demo.models.data.RightRoleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("Right")
@EnableAutoConfiguration
public class RightRoleController {
    @Autowired
    private RightRoleDao rightRoleDao;

    //Process for displaying list of rightRoles

    @RequestMapping(value = "display", method = RequestMethod.GET)
    @ResponseBody
    public Iterable<RightRole> displayerightRoleList() {
        return rightRoleDao.findAll();

    }

    //Process for adding rightRole to DB

    @RequestMapping(value = "add", method = RequestMethod.POST)
    @ResponseBody
    public RightRole processAddrightRole(@RequestBody(required = false) RightRole newRightRole) {
        rightRoleDao.save(newRightRole);
        return newRightRole;
    }

    //Process for removing rightRole from DB

    @RequestMapping(value = "remove/{rightRoleId}", method = RequestMethod.DELETE)
    @ResponseBody
    public RightRole processRemoverightRole(@PathVariable("rightRoleId") int rightRoleId) {
        RightRole newRightRole = rightRoleDao.findOne(rightRoleId);
        rightRoleDao.delete(rightRoleId);
        return newRightRole;
    }

    //Process for updating existing rightRole in DB
    @RequestMapping(value = "update", method = RequestMethod.PUT)
    @ResponseBody
    public RightRole processUpdaterightRole(@RequestBody(required = false) RightRole rightRole) {

        RightRole newrightRole = rightRoleDao.findOne(rightRole.getId());
        newrightRole.setName(rightRole.getName());
        rightRoleDao.save(newrightRole);

        return rightRole;
    }
}

package com.example.demo.models;

public class PowerForward implements PositionInterface{

    @Override
    public String getName() {
        return "Power Forward";
    }

    @Override
    public boolean checkLimits(int val) {
        if (val >= 190 && val <= 205) {
            return true;
        }
        return false;
    }
}

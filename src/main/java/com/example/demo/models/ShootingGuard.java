package com.example.demo.models;

public class ShootingGuard implements PositionInterface{

    @Override
    public String getName() {
        return "Shooting Guard";
    }

    @Override
    public boolean checkLimits(int val) {
        if (val >= 150 && val <= 185) {
            return true;
        }
        return false;
    }
}

package com.example.demo.models;

public class SmallForward implements PositionInterface{

    @Override
    public String getName() {
        return "Small Forward";
    }

    @Override
    public boolean checkLimits(int val) {
        if (val >= 185 && val <= 202) {
            return true;
        }
        return false;
    }
}

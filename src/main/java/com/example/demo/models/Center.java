package com.example.demo.models;

public class Center implements PositionInterface {


    @Override
    public String getName() {
        return "Center";
    }

    @Override
    public boolean checkLimits(int val) {
        if (val >= 190 && val <= 250) {
            return true;
        }
        return false;
    }
}

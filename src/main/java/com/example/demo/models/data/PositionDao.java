package com.example.demo.models.data;

import com.example.demo.models.Position;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface PositionDao extends CrudRepository<Position, Integer> {
}

package com.example.demo.models.data;

import com.example.demo.models.RightRole;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface RightRoleDao extends CrudRepository<RightRole, Integer> {
}

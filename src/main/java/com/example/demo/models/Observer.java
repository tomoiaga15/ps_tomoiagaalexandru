package com.example.demo.models;

import java.util.List;

public abstract class Observer {
    protected Player player;
    public abstract void updateTeam(Player players);
}

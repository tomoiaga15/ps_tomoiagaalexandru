package com.example.demo.models;

public class PointGuard implements PositionInterface {

    @Override
    public String getName() {
        return "Point Guard";
    }

    @Override
    public boolean checkLimits(int val) {
        if (val >= 175 && val <= 190) {
            return true;
        }
        return false;
    }
}

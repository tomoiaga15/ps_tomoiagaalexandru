package com.example.demo.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Position {

    @Id
    @GeneratedValue
    private int id;

    @NotNull
    @Size(min = 3, max = 15)
    private String name;

    @OneToMany
    @JoinColumn(name = "position_id")
    private List<Player> players = new ArrayList<>();

    public Position(String name){
        this.name = name;
    }

    public Position(){}

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public PositionInterface getPosition(int id){
        if(id == 15)
        {
            return new Center();
        }
        if(id == 12)
        {
            return new ShootingGuard();
        }
        if(id == 11)
        {
            return new PointGuard();
        }
        if(id == 10)
        {
            return new SmallForward();
        }
        if(id == 9)
        {
            return new PowerForward();
        }
        return null;
    }
}

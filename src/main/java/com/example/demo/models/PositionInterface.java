package com.example.demo.models;

public interface PositionInterface {

    String getName();
    boolean checkLimits(int val);
}

package com.example.demo;

import com.example.demo.models.*;
import com.example.demo.models.data.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PlayerTest {

    @Mock
    PlayerDao playerDao;
    @Mock
    PositionDao positionDao;

    private Player player;

    @Before
    public void init()
    {
        player = new Player("TestName", 1,"2.01", 21);
        player.setPosition(positionDao.findOne(15));

        when(playerDao.findOne(1)).thenReturn(player);
        when(playerDao.findOne(2)).thenReturn(null);
    }

    @Test
    public void addPlayer() {
        playerDao.save(player);
        assertEquals(player, playerDao.findOne(1));
    }

    @Test
    public void deletePlayer(){
        playerDao.delete(player);
        assertEquals(null, playerDao.findOne(2));
    }
}

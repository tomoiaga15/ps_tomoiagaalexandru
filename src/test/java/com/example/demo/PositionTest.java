package com.example.demo;

import com.example.demo.models.*;
import com.example.demo.models.data.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PositionTest {

    @Mock
    PositionDao positionDao;

    private Position position;

    @Before
    public void init()
    {
        position = new Position("TestName");

        when(positionDao.findOne(1)).thenReturn(position);
        when(positionDao.findOne(2)).thenReturn(null);
    }

    @Test
    public void addPosition(){
        positionDao.save(position);
        assertEquals(position, positionDao.findOne(1));
    }

    @Test
    public void deletePosition(){
        positionDao.delete(position);
        assertEquals(null, positionDao.findOne(2));
    }
}

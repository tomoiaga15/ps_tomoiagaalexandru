package com.example.demo;

import com.example.demo.models.*;
import com.example.demo.models.data.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TeamTest {

    @Mock
    TeamDao teamDao;

    private Team team;

    @Before
    public void init()
    {

        team = new Team("TestName");

        when(teamDao.findOne(1)).thenReturn(team);
        when(teamDao.findOne(2)).thenReturn(null);

    }

    @Test
    public void addTeam() {
        teamDao.save(team);
        assertEquals(team, teamDao.findOne(1));
    }

    @Test
    public void deleteTeam(){
        teamDao.delete(team);
        assertEquals(null, teamDao.findOne(2));
    }

}

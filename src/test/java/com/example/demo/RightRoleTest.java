package com.example.demo;

import com.example.demo.models.*;
import com.example.demo.models.data.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RightRoleTest {

    @Mock
    RightRoleDao rightRoleDao;

    private RightRole rightRole;


    @Before
    public void init()
    {
        rightRole = new RightRole("TestName");

        when(rightRoleDao.findOne(1)).thenReturn(rightRole);
        when(rightRoleDao.findOne(2)).thenReturn(null);
    }

    @Test
    public void addRight(){
        rightRoleDao.save(rightRole);
        assertEquals(rightRole, rightRoleDao.findOne(1));
    }

    @Test
    public void deleteRight(){
        rightRoleDao.delete(rightRole);
        assertEquals(null, rightRoleDao.findOne(2));
    }

}

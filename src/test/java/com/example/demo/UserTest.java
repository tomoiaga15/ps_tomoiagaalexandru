package com.example.demo;

import com.example.demo.models.*;
import com.example.demo.models.data.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserTest {

    @Mock
    UserDao userDao;

    private User user;

    @Before
    public void init()
    {

        user = new User("testEmail@email.com", "Name", "Last name");

        when(userDao.findOne(1)).thenReturn(user);
        when(userDao.findOne(2)).thenReturn(null);
    }

    @Test
    public void addUser(){
        userDao.save(user);
        assertEquals(user, userDao.findOne(1));
    }

    @Test
    public void deleteUser(){
        userDao.delete(user);
        assertEquals(null, userDao.findOne(2));
    }

}

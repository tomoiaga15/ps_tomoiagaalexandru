package com.example.demo;

import com.example.demo.models.*;
import com.example.demo.models.data.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	@Mock
	PlayerDao playerDao;
	@Mock
	PositionDao positionDao;
	@Mock
	TeamDao teamDao;
	@Mock
	UserDao userDao;
	@Mock
	RoleDao roleDao;
	@Mock
	RightRoleDao rightRoleDao;

	private Player player;
	private Position position;
	private Team team;
	private User user;
	private Role role;
	private RightRole rightRole;

	public DemoApplicationTests() {
	}

	@Before
	public void init()
	{
		player = new Player("TestName", 1,"2.01", 21);
		position = new Position("TestName");
		team = new Team("TestName");
		user = new User("testEmail@email.com", "Name", "Last name");
		role = new Role("TestName");
		rightRole = new RightRole("TestName");
		player.setPosition(positionDao.findOne(15));
		when(playerDao.findOne(1)).thenReturn(player);
		when(playerDao.findOne(2)).thenReturn(null);
		when(positionDao.findOne(1)).thenReturn(position);
		when(positionDao.findOne(2)).thenReturn(null);
		when(teamDao.findOne(1)).thenReturn(team);
		when(teamDao.findOne(2)).thenReturn(null);
		when(userDao.findOne(1)).thenReturn(user);
		when(userDao.findOne(2)).thenReturn(null);
		when(roleDao.findOne(1)).thenReturn(role);
		when(roleDao.findOne(2)).thenReturn(null);
		when(rightRoleDao.findOne(1)).thenReturn(rightRole);
		when(rightRoleDao.findOne(2)).thenReturn(null);
	}

	@Test
	public void addPlayer() {
		playerDao.save(player);
		assertEquals(player, playerDao.findOne(1));
	}

	@Test
	public void deletePlayer(){
		playerDao.delete(player);
		assertEquals(null, playerDao.findOne(2));
	}

	@Test
	public void addPosition(){
		positionDao.save(position);
		assertEquals(position, positionDao.findOne(1));
	}

	@Test
	public void deletePosition(){
		positionDao.delete(position);
		assertEquals(null, positionDao.findOne(2));
	}

	@Test
	public void addTeam() {
		teamDao.save(team);
		assertEquals(team, teamDao.findOne(1));
	}

	@Test
	public void deleteTeam(){
		teamDao.delete(team);
		assertEquals(null, teamDao.findOne(2));
	}

	@Test
	public void addUser(){
		userDao.save(user);
		assertEquals(user, userDao.findOne(1));
	}

	@Test
	public void deleteUser(){
		userDao.delete(user);
		assertEquals(null, userDao.findOne(2));
	}

	@Test
	public void addRole() {
		roleDao.save(role);
		assertEquals(role, roleDao.findOne(1));
	}

	@Test
	public void deleteRole(){
		roleDao.delete(role);
		assertEquals(null, roleDao.findOne(2));
	}

	@Test
	public void addRight(){
		rightRoleDao.save(rightRole);
		assertEquals(rightRole, rightRoleDao.findOne(1));
	}

	@Test
	public void deleteRight(){
		rightRoleDao.delete(rightRole);
		assertEquals(null, rightRoleDao.findOne(2));
	}

}

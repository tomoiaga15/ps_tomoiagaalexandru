package com.example.demo;

import com.example.demo.models.*;
import com.example.demo.models.data.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RoleTest {

    @Mock
    RoleDao roleDao;

    private Role role;

    @Before
    public void init()
    {
        role = new Role("TestName");

        when(roleDao.findOne(1)).thenReturn(role);
        when(roleDao.findOne(2)).thenReturn(null);
    }

    @Test
    public void addRole() {
        roleDao.save(role);
        assertEquals(role, roleDao.findOne(1));
    }

    @Test
    public void deleteRole(){
        roleDao.delete(role);
        assertEquals(null, roleDao.findOne(2));
    }
}
